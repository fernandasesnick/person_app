#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>

#include "../include/person.h"
#include "../include/test_module.h"

int main(void) {
	CU_pSuite pSuite = NULL;

	if (CUE_SUCCESS != CU_initialize_registry())
	{
		return (CU_get_error());
	}
	pSuite=CU_add_suite("Suite 1",person_init_test,person_deinit_test);
	if (NULL==pSuite)
	{
		CU_cleanup_registry();
		return (CU_get_error());
	}

	if ((NULL==CU_add_test(pSuite, "Create person test", person_create_test)) ||
		(NULL==CU_add_test(pSuite, "load from file", person_load_from_file_test)) ||
		(NULL==CU_add_test(pSuite, "sort the list", person_bubble_sort_test))  ||
		(NULL==CU_add_test(pSuite, "find first person by age", person_find_by_age_test)) ||
		(NULL==CU_add_test(pSuite, "find first person by name", person_find_by_name_test))||
		(NULL==CU_add_test(pSuite, "find first person by state", person_find_by_state_test))||
		(NULL==CU_add_test(pSuite, "find first person by died", person_find_by_died_test))||
		(NULL==CU_add_test(pSuite, "create a person", person_create_test)) ||
		(NULL==CU_add_test(pSuite, "add a person", person_add_test))  ||
		(NULL==CU_add_test(pSuite, "remove first person", person_remove_head_test)) ||
		(NULL==CU_add_test(pSuite, "Get first person", person_first_test))||
		(NULL==CU_add_test(pSuite, "Get last person", person_last_test)))
	{

		CU_cleanup_registry();
		return (CU_get_error());
	}

	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	CU_cleanup_registry();
	return (CU_get_error());
}
