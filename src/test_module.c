#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CUnit/Basic.h>

#include <person.h>
#include <test_module.h>

#define NAME "John Lennon"
#define AGE 40
#define DIED "NEW YORK"
const enum PersonState STATE = MARRIED;

int person_init_test(void)
{
	return person_init() == EXIT_SUCCESS ? 0 : 1;
}

int person_deinit_test(void)
{
	return person_deinit() == EXIT_SUCCESS ? 0 : 1;
}

void person_create_test(void)
{

	struct Person* test_person = person_create(NAME, AGE, STATE, DIED);

	CU_ASSERT_PTR_NOT_NULL(test_person);

	if(NULL != test_person)
	{
		//expected behaviour
		CU_ASSERT_STRING_EQUAL(NAME, test_person->name);
		CU_ASSERT_STRING_EQUAL(DIED,test_person-> died)
		CU_ASSERT_EQUAL(test_person->state, STATE);
		CU_ASSERT_EQUAL(test_person->age,AGE);

		//UNEXPECTED BEHAVIOUR

		test_person = person_create(NULL, AGE, STATE, DIED);
		//CU_ASSERT_PTR_NOT_NULL(test_person);

		test_person = person_create(NAME, -1, STATE, DIED);
		//CU_ASSERT_PTR_NULL(test_person);

		test_person = person_create(NAME, AGE, -1, DIED);
		//CU_ASSERT_PTR_NULL(test_person);

		test_person = person_create(NAME, AGE, NOT_VALID +1, DIED);
		//CU_ASSERT_PTR_NULL(test_person);

#ifdef STRICT

		test_person = person_create(NAME, AGE, NOT_VALID, NULL);
		//ASSERT_PTR_NULL(test_person);

		test_person = person_create(NAME, AGE, NOT_VALID, "");
		//CU_ASSERT_PTR_NULL(test_person);
#endif

		CU_PASS("Add person end successfully");
	}
}

void person_load_from_file_test(void)
{
	CU_ASSERT_EQUAL(person_load_from_file("vh.txt"), EXIT_SUCCESS);
}


void person_add_test(void)
{
	struct Person* test_person = person_create(NAME, AGE, STATE, DIED);
	struct Person* head = person_first()	;

	CU_ASSERT_PTR_NOT_NULL(head);
	CU_ASSERT_EQUAL(person_add(test_person), EXIT_SUCCESS);

	CU_ASSERT_EQUAL(test_person, person_find_by_name(NAME, head));

}


void person_remove_head_test(void)
{
	struct Person *Head = person_first();
	if(NULL != Head)
	{
		CU_ASSERT_PTR_NOT_NULL(person_remove_head());
	}
	CU_ASSERT_NOT_EQUAL(Head, person_find_by_name(Head->name, Head));
}

void person_find_by_name_test(void)
{
	struct Person *person_head = person_first();
	CU_ASSERT_PTR_NOT_NULL(person_head);
	if(NULL != person_head)
	{
		struct Person* Search_Person = person_find_by_name("Zsoldos Sándor ", person_head);
		CU_ASSERT_PTR_NOT_NULL(Search_Person);
		CU_ASSERT_EQUAL(strcmp("Zsoldos Sándor ", Search_Person->name), 0);

		Search_Person = person_find_by_name("Not Found Name", person_head);
		CU_ASSERT_PTR_NULL(Search_Person);
	}

}

void person_find_by_age_test(void)
{
	struct Person *person_head = person_first();
	CU_ASSERT_PTR_NOT_NULL(person_head);
	if(NULL != person_head)
	{
		struct Person* Search_Person = person_find_by_age(31, person_head);
		CU_ASSERT_PTR_NOT_NULL(Search_Person);
		CU_ASSERT_EQUAL(31, Search_Person->age);

		Search_Person = person_find_by_age(200, person_head);
		CU_ASSERT_PTR_NULL(Search_Person);
	}

}

void person_find_by_state_test(void)
{
	struct Person *person_head = person_first();
	CU_ASSERT_PTR_NOT_NULL(person_head);
	if(NULL != person_head)
	{
		struct Person* Search_Person = person_find_by_state(SINGLE, person_head);
		CU_ASSERT_PTR_NOT_NULL(Search_Person);
		CU_ASSERT_EQUAL(SINGLE, Search_Person->state);

		Search_Person = person_find_by_state(NOT_VALID, person_head);
		CU_ASSERT_PTR_NULL(Search_Person);
	}

}


void person_find_by_died_test(void)
{
	struct Person *person_head = person_first();
	CU_ASSERT_PTR_NOT_NULL(person_head);
	if(NULL != person_head)
	{
		struct Person* Search_Person = person_find_by_died("Lengyelország", person_head);
		CU_ASSERT_PTR_NOT_NULL(Search_Person);
		CU_ASSERT_EQUAL(strcmp("Lengyelország", Search_Person->died), 0);

		Search_Person = person_find_by_died("Alexandria", person_head);
		CU_ASSERT_PTR_NULL(Search_Person);
	}
}


void person_first_test(void)
{
	struct Person *person_head = person_first();
	struct Person *person_test = person_first();
	CU_ASSERT_EQUAL(strcmp(person_head->name, person_test->name), 0);
	CU_ASSERT_PTR_NOT_NULL(person_first());
}


void person_last_test(void)
{
	struct Person *person_last_test = person_last ();
	struct Person *person_test = person_last();

	CU_ASSERT_EQUAL(strcmp(person_last_test->name, person_test->name), 0);
	CU_ASSERT_PTR_NOT_NULL(person_last());
}

void person_bubble_sort_test(void)
{
	/* sort the list ascending by any comparer ex: age */
	person_bubble_sort(ASCENDING,person_compare_by_age);
	/* when compare between any element and next one  */
	struct Person* prevNode = person_first();
	struct Person* currentNode = TAILQ_NEXT(prevNode, next);
	/* loop on the list and assert if any error found */
    while(currentNode)
	{
    	CU_ASSERT_FALSE((prevNode->age) > (currentNode->age));
		prevNode = currentNode;
		currentNode = TAILQ_NEXT(prevNode, next);
	}
}


