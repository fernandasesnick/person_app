/*
 * person.c
 *
 *  Created on: Feb 24, 2020
 *      Author: fernanda
 */

#include "../include/person.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <../include/test_module.h>
#include <person.h>


static const size_t BUFFER_SIZE=1024;
//static char *strdup(char *string);

static struct PersonRepository {
	int initialized;
	TAILQ_HEAD(Person_list, Person) head;
} person_repository = { .initialized=0 };


int person_init(){
	TAILQ_INIT(&person_repository.head);
	person_repository.initialized=1;
	return EXIT_SUCCESS;
}

int person_deinit(){
	if (!person_repository.initialized)
	   return (EXIT_FAILURE);

	struct Person *p;
	while((p=person_remove_head())) {
		FREE(p);
	}
	return EXIT_SUCCESS;
}


char *str_chomp(char *line) {
	if (!(line && *line))
		return line;

	char *begin=line;
	while (*line++)
		;

	while (--line>=begin) {
		if (*line >= ' ')
			return begin;

		if (*line == '\r' || *line == '\n') // \r = 0x0a \n=0xd
			*line = '\0';
	}

	return begin;
}

struct Person* person_create(char *name, int age, int statement, char *died) {
	if (!(name && *name && age >= 0 && age<= MAX_AGE && statement >= SINGLE && statement <= NOT_VALID

#ifdef STRICT
			&& died &&*died
#endif
	))
		return NULL;

	struct Person *result;
	MALLOC(result, sizeof(struct Person));

	result->name = name;
	result->age=age;
	result->state = statement;
	result->died=died;
	//result->next.tqe_next = NULL;

	return result;
}

/**
 *
 * \return new allocated memory, caller must free it!
 */
struct Person *person_process_file_line(char *line) {
	if (!line || !*line) //empty line
		return NULL;

	struct Person* result = NULL;
	char *n = strtok(line, DELIMITER); // name
	char *a = strtok(NULL, DELIMITER); // age
	char *st = strtok(NULL, DELIMITER); // state
	char *d = strtok(NULL, DELIMITER); // died

#ifdef STRICT  // strict checking every field have to have value
	if (n && *n && a && *a && st && *st && d && *d) //
		result = person_create(strdup(n), // copy
		atoi(a),  // to int
		atoi(st), // to state
		strdup(d)); //copy
#else  // at least name exists
	if (n && *n)
	result = person_create(strdup(n), // copy
			a ? atoi(a) : 0,// ok, or 0
			st ? atoi(st) : -1,// ok or -1
			d ? strdup(d): "?");// copy or ?
#endif
	return (result);
}

int person_load_from_file(const char *name){
	printf("enter person_load_from_file, name:%s ",name);
	if (!(name && *name))
		return EXIT_FAILURE;

	FILE *f=fopen(name, READ_MODE);
	if (!f)
		return EXIT_FAILURE;

	char *line;
	MALLOC(line, BUFFER_SIZE);

	struct Person* person = NULL;


	while((line=fgets(line, BUFFER_SIZE, f))) {
		person = person_process_file_line(str_chomp(line));
		person_add(person);
	}
	/*print all persons from the list if needed*/
    //person_print_all(person_first());

	FREE(line);
	fclose(f);
	return EXIT_SUCCESS;
}

int person_add(struct Person *person){
	if(!(person_repository.initialized && person))
		return EXIT_FAILURE;

	TAILQ_INSERT_TAIL(&person_repository.head, person, next);
	return EXIT_SUCCESS;
}

struct Person *person_remove_head(void)
{
	if ((!person_repository.initialized) || (TAILQ_EMPTY(&person_repository.head)) )
	{
		return NULL;
	}
	static struct Person *p = NULL;
	p = TAILQ_FIRST(&person_repository.head);

	TAILQ_REMOVE(&person_repository.head, p, next);
	return p;
}

char *person_get_state(enum PersonState state)
{
	/* return string to represent the person state to be used in print function */
	switch (state)
	{
        case DIVORCED :
			return ("Divorced\n");
        case SINGLE :
			return ("Single\n");
		case MARRIED :
	 		return ("Married\n");
        case WIDOW :
			return ("Widow\n");
        default :
			return ("Please specify a correct state\n");
    }
}

void person_print_a_person(struct Person *person)
{
	if (NULL == person)
	{
		printf("Person is not defined\n");
		//return NULL;
	}
	else

	printf("Name:%s, age:%d, State:%s, Died at:%s\n", \
	person->name, person->age, person_get_state(person->state), person->died ? person->died : "?");
}

void person_print_all(struct Person *find)
{
	if (!person_repository.initialized)
	{
		return;
	}
	struct Person* personToPrint;

	TAILQ_FOREACH(personToPrint, &person_repository.head, next)
		person_print_a_person(personToPrint);
}

struct Person *person_find_by_name(const char *name, struct Person *find)
{
	 if (! (find && name && *name && person_repository.initialized))
	 {
        return NULL;
	 }

    struct Person* p = find;
	/* linearly search until a person with matched name found or list is ended*/
    while((p=TAILQ_NEXT(p, next)))
	{
    	if (strcmp(p->name, name)==0)
		{
    		return (p);
		}
	}

    return NULL;
}

struct Person *person_find_by_age(int age, struct Person *find)
{
	/* check if any of pointer passed is NULL and persons tail queue is initialized*/
	 if (! (find && person_repository.initialized))
	 {
        return NULL;
	 }

    struct Person* p = find;
    while((p=TAILQ_NEXT(p, next)))
	{
    	if (p->age == age)
		{
    		return (p);
		}
	}

    return NULL;

}

struct Person *person_find_by_state(enum PersonState state, struct Person *find)
{
	/* check if any of pointer passed is NULL and persons tail queue is initialized*/
	 if (! (find && person_repository.initialized))
	 {
        return NULL;
	 }

    struct Person* p = find;
    while((p=TAILQ_NEXT(p, next)))
	{
    	if (p->state == state)
		{
    		return (p);
		}
	}

    return NULL;

}

struct Person *person_find_by_died(const char *died, struct Person *find)
{
	/* check if any of pointer passed is NULL and persons tail queue is initialized*/
	 if (! (find && died && *died && person_repository.initialized))
	 {
        return NULL;
	 }

    struct Person* p = find;
    while((p=TAILQ_NEXT(p, next)))
	{
    	if (strcmp(p->died, died)==0)
		{
    		return (p);
		}
	}

    return NULL;

}

struct Person *person_first(void)
{
	return (person_repository.initialized
			? TAILQ_FIRST(&person_repository.head)
			: NULL);
}

struct Person *person_last()
{
	return (person_repository.initialized
			? TAILQ_LAST(&person_repository.head, Person_list)
			: NULL);
}

void person_bubble_sort(enum Ordering ordering,
			int (*comparer)(const struct Person *a, const struct Person *b))
{

	if (!comparer)
		return;

	int change=0;
	do {
		change = 0;
		struct Person *p1=TAILQ_FIRST(&person_repository.head);
		struct Person *p2=TAILQ_NEXT(p1, next);
		while(p1&&p2) {
			int result = comparer(p1,p2);
			if (((ordering==ASCENDING)&&(result>0))
				|| ((ordering==DESCENDING)&&(result<0))) {
				TAILQ_REMOVE(&person_repository.head, p1, next);
				TAILQ_INSERT_AFTER(&person_repository.head, p2, p1, next);
				change++;
				p2 = TAILQ_NEXT(p1, next);
			}
			else {
				p1=p2;
				p2=TAILQ_NEXT(p2, next);
			}
		}
	}while(change);
}

int person_compare_by_name(const struct Person *a, const struct Person *b){
		return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			strcmp(a->name, b->name));
}

int person_compare_by_age(const struct Person *a, const struct Person *b){
		return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			a->age-b->age);
}

int person_compare_by_state(const struct Person *a, const struct Person *b){
	return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			a->state-b->state);
}

int person_compare_by_died(const struct Person *a, const struct Person *b){
		return (a==b ? 0 :
			!a&&b ? -1 :
			!b&&a ? 1 :
			 !a->died&&b->died ?  -1 :
			 !b->died&&a->died ? 1 :
			 strcmp(a->died, b->died));
}

//static char *strdup(char *string)
//{
//    char *ptr;
//    int len = 0;
//
//    len = strlen(string);
//    MALLOC(ptr,len+1);
//
//	strcpy(ptr,string);
//	FREE(ptr);
//	return ptr;
//}
