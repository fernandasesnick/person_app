SRC=src
BIN=bin

CFLAGS=-Wall -g -pedantic --std=c11 -D_DEFAULT_SOURCE

OBJS=$(patsubst $(SRC)/%.c,$(BIN)/%.o,$(wildcard $(SRC)/*.c))

$(info src:$(wildcard $(SRC)/*.c))

$(info *objs:$(OBJS))
 
$(BIN)/test: $(BIN) $(OBJS)
	gcc -o bin/test $(OBJS) -l cunit -g 

$(BIN):
	mkdir $(BIN)

$(BIN)/%.o:$(SRC)/%.c
	gcc -o $@  -c $< -I include $(CFLAGS)
					
#obj/main.o:src/main.c	
#	gcc -c -I./include src/main.c -o obj/main.o $(CFLAGS)
#
#obj/test_module.o: src/test_module.c
#	gcc -c -I./include src/test_module.c -o obj/test_module.o $(CFLAGS)
#
#obj/person.o: src/person.c
#	gcc -c -I./include src/person.c -o obj/person.o $(CFLAGS)

clean:
	rm -rf bin
	rm -rf obj

all: $(BIN)/test

test: all
	$(BIN)/test
	
.PHONY: clean all
	