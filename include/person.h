/**
 * Brief Person administration inside a TAILQ
 *
 * Implementing a TAILQ to store person data
 *
 * \Author Fernanda
 * \ Version 2.0
 */


#ifndef INCLUDE_PERSON_H_
#define INCLUDE_PERSON_H_

#include <sys/queue.h>

#define MALLOC(ptr,size) 	\
	do {					\
		ptr=malloc(size); 	\
		if (!ptr) 			\
			abort(); 		\
	} while(0)

#define FREE(ptr) 			\
	do {					\
		free(ptr);			\
		ptr=NULL;			\
	} while(0)
/**
 * If is defined we check everything like no die can be empty
 *
 */
#define STRICT
#define READ_MODE "r"
#define DELIMITER ";"
/**
 * Maximum age of a person, it is changeable
 */
#define MAX_AGE 100
/**
 * \enum PersonState
 */
enum PersonState {
	///Single person
	SINGLE=0,
	/// Maried person
	MARRIED=1,
	///Divorced person
	DIVORCED=2,
	///Widow person
	WIDOW=3,
	///Unknown
	NOT_VALID = 4
};
/**
 * \enum Ordering
 */
enum Ordering {
	ASCENDING,
	DESCENDING
};
/**
 * \Brief we can store people with this structure
 */
struct Person {
	///Name of the person, can't be NULL
	char *name;
	int age;
	enum PersonState state;
	char *died;
	/// Next element just for TAILQ
	TAILQ_ENTRY(Person) next;
};
/**
 * Brief init person module
 *
 * Needed to call this, if you forget to call it will have unexpected results
 *
 * \result EXIT_SUCCESS if successfully or EXIT_FAILURE case something wrong
 *
 */
int person_init();
/**
 * \Brief deinit module
 *
 * free all of the occupied resources like memory
 *
 ** \result EXIT_SUCCESS if successfully or EXIT_FAILURE case something wrong
 */
int person_deinit();
/**
 * \brief person load from file module
 *
 * load person from txt file
 *
 * \result EXIT_SUCCESS,It allocate memory, must call free
 */
int person_load_from_file(const char *name);
/**
 * \brief create a new person struct based on arguments
 *
 * \param name of person, it cant be NULL
 * \param age of person, it must be between 0 till MAX_AGE
 * \param statement state of the person, expect only valid
 * \param died can be NULL if STRICT not defined
 * \result created new person structure or NULL if something wrong. It allocate memory, must call free
 *
 */
struct Person* person_create(char *name, int age, int statement, char *died);
/**
 * \brief process line from file
 *
 * \param line can't be an empty line
 * \result a new person struct
 */
struct Person *person_process_file_line(char *line);
/**
 * \brief add a new person in the reposiroty
 *
 * \param a type struct Person
 * \result EXIT_SUCCESS if successfully or EXIT_FAILURE case something wrong
 */
int person_add(struct Person *person);
/**
 * \bried remove head module
 *
 * remove the person on the head TAILQ
 *
 * \result return the repository without the head or NULL if is empty
 */
struct Person *person_remove_head();
/**
 * \brief person get state module
 *
 * return string to represent the person state to be used in print function
 *
 * \param state to get the person state
 *
 * \result Divorced, single, married, widow, Please specify a correct state if not right entry
 */
char *person_get_state(enum PersonState state);
/**
 * \brief person print a person
 *
 * \param person to be printed
 *
 */
void person_print_a_person(struct Person *person);
/**
 * \bried person print all the repository
 *
 * \param find takes the repository with the persons using TAILQ
 *
 */
void person_print_all(struct Person *find);
/**
 * \brief find a person by parameter name
 *
 * Linearly search until a person with matched name found or list is ended
 *
 * \param name of person, it cant be NULL
 * \param from the list where you want to find the name
 *
 * \result Person with that name, NULL if something is wrong
 */
struct Person *person_find_by_name(const char *name, struct Person *from);
/**
 * \brief find a person by parameter age
 *
 * Check if any of pointer passed is NULL and persons tail queue is initialized
 *
 * \param age of person,
 * \param from the list where you want to find the name
 *
 * \result Person with that age, NULL if something is wrong
 */
struct Person *person_find_by_age(int age, struct Person *from);
/**
 * \brief find a person by parameter state
 *
 * check if any of pointer passed is NULL and persons tail queue is initialized
 *
 * \param state of person,
 * \param from the list where you want to find the name
 *
 * \result Person with that state, NULL if something is wrong
 */

struct Person *person_find_by_state(enum PersonState state, struct Person *from);
/**
 * \brief find a person by parameter died
 *
 * check if any of pointer passed is NULL and persons tail queue is initialized
 *
 * \param died of person,
 * \param from the list where you want to find the name
 *
 * \result Person with that place of death, NULL if something is wrong
 */
struct Person *person_find_by_died(const char *died, struct Person *from);
/**
 * \Brief person first module
 *
 ** \result  first person in the list NULL case something wrong
 */
struct Person *person_first();
/**
 * \Brief person last module
 *
 ** \result  last person in the list NULL case something wrong
 */
struct Person *person_last();
/**
 * \brief bubble sort
 *
 * ordering the list
 *
 * \param enum ASCENDING or DESCENDING
 * \param comparer function to compare by age, name, state or died
 * \param a person
 * \param b person
 *
 */
void person_bubble_sort(enum Ordering ordering,
			int (*comparer)(const struct Person *a, const struct Person *b));
/**
 * \brief compare two persons by name
 *
 * \param a person struct
 * \param b person struct
 *
 * \result 0, 1 , -1
 */
int person_compare_by_name(const struct Person *a, const struct Person *b);
/**
* \brief compare two persons by age
*
* \param a person struct
* \param b person struct
*
* \result 0, 1 , -1
*/
int person_compare_by_age(const struct Person *a, const struct Person *b);
/**
* \brief compare two persons by state
*
* \param a person struct
* \param b person struct
*
* \result 0, 1 , -1
*/
int person_compare_by_state(const struct Person *a, const struct Person *b);
/**
* \brief compare two persons by died
*
* \param a person struct
* \param b person struct
*
* \result 0, 1 , -1
*/
int person_compare_by_died(const struct Person *a, const struct Person *b);

#endif /* INCLUDE_PERSON_H_ */
