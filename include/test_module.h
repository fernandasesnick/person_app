#ifndef CUNITTEST_H_
#define CUNITTEST_H

#include <stdint.h>

int person_init_test(void);

int person_deinit_test(void);

void person_create_test(void);

void person_load_from_file_test(void);

void person_add_test(void);

void person_remove_head_test(void);

void person_find_by_name_test(void);

void person_find_by_age_test(void);

void person_find_by_state_test(void);

void person_find_by_died_test(void);

void person_first_test(void);

void person_last_test(void);

void person_bubble_sort_test(void);

#endif

